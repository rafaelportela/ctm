# 3-  Declarative programming techniques

* Declarative programs are compositional - they consist of components that can each be written,
tested, and proved correct independently of other components and fo its own past history (previous
calls).

* Reasoning about declarative programs is simple - it's easier then other more expressive models.
Since declarative programs compute only values, simle algebraic and logical reasoning techniques
can be used.

## 3.1- What is declarativeness?

Intuitively, it is programming be defining the _what_ (the results we eant to achive) without
explaining the _how_ (the algorithms, etc., needed to achieve the results).

*Descriptive* declarativeness. This is the least expressive. The declarative "program" just defines
a data structure. Examples are formatting languages like HTML and information exchange languages
like XML. The descriptive level is too weak to write general programs, but are interesting because
it consists of data structures that are easy to calculate with.

*Programmable* declarativeness. More expressive, can run virtually any kind of computation as a
Turing Machine. It can be viewed on 2 different ways:

* A _definitional_ view, where declarativeness is a property of the componenet implementation. E.g.
programs written in the declarative model from previous chapter. Functional and logical programming
are two styles of definitional declarative programming.

* An _observational_ view, where declarativeness is a property of the component interface. The
component just has to behave declaratively, i.e., as if it were independent, stateless, and
deterministic, without necessarily being written in a declarative computation model. For example, a
database interface can be a valuable addition to a declarative language, yet the implementation of
this interface is almost certainly not going to logical or functional. It suffices that it could
have been defined declaratively.

## 3.2- Iterative computation

We will now look at how to program in the declarative model. We start by looking at a very simple
kind of program, the _iterative computation_. An iterative computation is a loop whose stack size is
bounded by a constant, independent of the number of iterations.

It defines a procedure based on `Iterate`, `IsDone` and `Transform` to build a controle flow
abstraction. If such control abstraction is used often, the next step bould be to provide it as a
linguistic abstraction.

## 3.4- Programming with recursion

Lots and lots of sections and examples with data structures following a declarative model. Basically
covering them on a functional programming style based on the initial kernel language.

## 3.5- Higher-order programming

* Procedural abstraction
* Genericity
* Instantiation
* Embedding

Declarative versus imperative loops

In C or Java, the loop counter is an assignable variable which is assigned a different value on each
iteration. The declarative loop is quite different: on each iteration it declares a _new_ variable.
All these vaeiables are referred to by the _same_ identifier. This difference can have major
consequences. For example, the iterations of a declarative loop are completely independent of each
other, therefore, it is possible to run them concurrently without changing the loop's final result.

