# Concepts, Techniques, and Models of Computer Programming

Install Mozart (programming environment) and Oz (programming languagem) from
https://mozart.github.io/ .

Example `hello.oz` file:
```
functor
import
    System
    Application
define
    {System.show 'Hello World!'}
    {Application.exit 0}
end
```

Compile and run:
```
$ ozc -c hello.oz
$ ozengine hello.ozf
```

