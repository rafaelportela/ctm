# 2- Declarative Computation Model

## 2.2- The single-assignment store

Declarative variables that cause the program to wait until they are bound are called _dataflow_
variables. The declarative model uses dataflow variables because they are tremendously useful in
concurrent programming.A

If two concurrent operations, say `A=23` and `B=A+1`, then it will always run correctly and give the
answer `B=24`. This property of _order-independence_ makes possible the declarative concurrency.

## 2.4- Kernel language semantics

*Procedures with external references*

What is the value of Y? It is not one of the procedure arguments. It has to be the value of Y _when
the procedure is defined_. This is a consequence of static scoping.

*Dynamic scoping versus static scoping*

Which default is better? The correct default is procedure values with static scoping. This is
because a procedure that works when it is defined will continue to work, independent of the
environment where it is called. This is an important software engineering prope

*Dataflow behavior*

[In a given IF statement] If Y is unbound, it cannot decide true or false, strictly speaking.
Continuing with either value would be incorrect. Raisingn an error would be a drastic measure. We
decide that the program will simply stop its execution, without signaling any kind of error. If some
other activity binds Y then the stopped execution can continue as if nothing had perturbed the
normal flow of execution. This is called _dataflow behavior_, if underlies a powerful tool, namely
_concurrency_.

