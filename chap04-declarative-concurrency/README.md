# 4-  Declarative Concurrency

## 4.3- Streams

The most usefull technique for concurrent programming in the declarative concurrent model is using
streams to communicate between threads. A _stream_ is a pontentially unbounded list of messages, it
is a list whose tail is an unbound dataflow variable.

Sending a message is done by extending the stream by one element: bind the tail to a list pair
containing the message and a new unbound tail. REceiving a message is reading a stream element.

A thread communication through streams is a kind of "active object" that we will call a _stream
object_. No locking or mutual exclusion is necessary since each variable is bound by only one
thread.

Stream programming is a quite generatl approach that can be applied in many domains. Is is the
concept underlying Unix pipes. This chapter looks at a special case of stream programming, namely
_deterministic_ stream programming, in which each stream object always knows for each input where
the message will come from. This case is interesting because it is declarative. Yet it is already
quite useful. We put off looking at nondeterministic stream programming until Chapter 5.

### 4.3.2- Transducers and pipelines

We can put a third stream object in between the producer and consumer. This stream object reads the
producer's stream and creates another stream which is read by the consumer. We call it a
_transducer_. In general, a sequence of stream objects each of which feeds the next is called a
_pipeline_.

The producer is sometimes called the _source_ and the consumer is sometimes called the _sink_.

One of the simplest transducers is the _filter_.

### 4.3.3- Managing resources and improving throughput

What happens if the producer generates elements faster than the consumer can consume them? One way
to solve this problem is to limit the rate at which the producer generates new elements. This is
called _flow control_. It requires that some information be sent back from the consumer to the
producer.

*Flow control with demand-driven concurrency*

Or _lazy execution_. In this technique, the producer only generates elements when the consumer
explicity demands them. The previous technique, where the producer generates an element whenever it
likes, is called _supply-driven_ or _eager execution_.

Lazy execution requires a mechanism for the consumer to signal the producer whener it needs a new
element. The simplest way to do this is to use dataflow [client code blocks if variable is not bound
yet]. The consumer can extend its input stream whenever it needs a new element. That is, the
consumer binds the stream's end to a list pair X|Xr, where X is unbound. The producer waits for this
list pair and then binds X to the next element.

*Flow control with a bounded buffer*

We have seen that eager execution leads to an explosion in resource usage. But Lazy execution also
has a serious problem. It leads to a strong reduction in throughput. If the consumer requests a
message, then the producer has to calculate it, and meanwhile, the consumer waits. If the producer
were allowed to get ahead of the consumer, then the consumer would not have to wait.

A _bounded buffer_ is a transducer that stores elements up to a maximum number, say n. The producer
is allowed to get ahead of the consumer, but only until the buffer is full. The consumer can take
elements from the c=buffer immediately without waiting. This keeps throughput high.

Eager execution is what happens when the buffer has infinite size. Lazy execution is what happens
when the buffer has zero size.

*Flow control with thread priorities*

A different and inferior way to do flow control is to change the relative priorities between
producer and consumer threads, so that consumers consume faster than producers can produce. It's
fragile, its success depends on the amount of work needed for an element to be produced and
consumed.

Then general lesson is that changing thread priorities should _never_ be used to get a program to
work correctly. Changing thread priorities is then a performance optimization; it can be used to
improve the throughput of a program that is already working.
